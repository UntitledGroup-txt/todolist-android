package com.bil495.todolist;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class MainActivity extends AppCompatActivity {
    static int status;
    static int userID;
    public static final String EXTRA_MESSAGE = "com.bil495.todolist.MA";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /* Reach UI elements */
        final EditText usernameField = (EditText) findViewById(R.id.username);
        final EditText passwordField = (EditText) findViewById(R.id.password);
        Button registerPageButton = (Button) findViewById(R.id.registerPageButton);
        Button loginButton = (Button) findViewById(R.id.login);

        registerPageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), Register.class);
                startActivity(intent);
            }
        });


        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String username = usernameField.getText().toString();
                String password = passwordField.getText().toString();
                if(username.equals("") || password.equals("")){
                    Toast.makeText(MainActivity.this,"Fields cannot be empty", Toast.LENGTH_SHORT).show();
                }
                else{
                    Information info = new Information(username,password,"","","","","",-1,-1);
                    ParameterType param = new ParameterType(5,null,info); // httpRequestMode = 5 for login
                    try {
                        new ServerConnection().execute(param).get();    // to wait until doInBackground finishes it's job
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } catch (ExecutionException e) {
                        e.printStackTrace();
                    }

                    if(status == 1){    // if user successfully login, then redirect home page with his/her credential(as user id)
                        Intent intent = new Intent(v.getContext(), UserHomePage.class);
                        intent.putExtra("userID",userID);
                        intent.putExtra("username",username);
                        usernameField.setText("");
                        passwordField.setText("");
                        startActivity(intent);
                    }
                    else{
                        Toast.makeText(MainActivity.this,"Wrong username of password", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    public static void getResponse(int status, int userID){
        // postExecute (doInBackground's) call this method and set status
        // 1 means status is returned as successful by server. And get userID to use it while fetching lists and tasks
        MainActivity.status = status;
        MainActivity.userID = userID;
        System.out.println("status in getResponse: "+status+" - userID in getResponse: "+userID);

    }
    }

