package com.bil495.todolist;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.concurrent.ExecutionException;

/**
 * Created by Ertuğrul Güngör on 02.08.2017.
 */

public class Register extends AppCompatActivity {
    static int status;
    public static final String LOG_MESSAGE = "Register";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register);

        final EditText usernameField = (EditText) findViewById(R.id.registerUsername);
        final EditText passwordField = (EditText) findViewById(R.id.registerPassword);
        final EditText passwordAgainField = (EditText) findViewById(R.id.registerPasswordAgain);
        final EditText emailField = (EditText) findViewById(R.id.email);
        final EditText nameField = (EditText) findViewById(R.id.name);
        final EditText surnameField = (EditText) findViewById(R.id.surname);
        Button registerButton = (Button) findViewById(R.id.registerButton);

        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String username = usernameField.getText().toString();
                String password = passwordField.getText().toString();
                String passwordAgain = passwordField.getText().toString();
                String email = passwordField.getText().toString();
                String name = passwordField.getText().toString();
                String surname = passwordField.getText().toString();

                /* Fields should not be empty */
                if(TextUtils.isEmpty(username) || TextUtils.isEmpty(password)|| TextUtils.isEmpty(passwordAgain)
                        || TextUtils.isEmpty(name) || TextUtils.isEmpty(surname) || TextUtils.isEmpty(email)){
                    Toast.makeText(v.getContext(),"Fields cannot be empty", Toast.LENGTH_SHORT).show();
                }
                else{
                    Information info = new Information(username,password,passwordAgain,
                            name,surname,email,"",-1,-1);
                    ParameterType param = new ParameterType(4,null,info); // httpRequestMode = 4 for registration
                    try {
                        new ServerConnection().execute(param).get();        // to wait until doInBackground finishes it's job
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } catch (ExecutionException e) {
                        e.printStackTrace();
                    }

                    if (status==1){ // if registration is successful then redirect user to login page
                        Intent intent = new Intent(v.getContext(), MainActivity.class);
                        startActivity(intent);
                    }
                    else{
                        usernameField.setText("");
                        passwordField.setText("");
                        passwordAgainField.setText("");
                        emailField.setText("");
                        nameField.setText("");
                        surnameField.setText("");
                        Toast.makeText(Register.this,"Registration cannot be completed. Please try again!", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }
    public static void getResponse(int status){
        // postExecute (doInBackground's) call this method and set status
        // 1 means status is returned as successful by server.
        Register.status = status;
    }

}
