package com.bil495.todolist;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by Ertuğrul Güngör on 11.07.2017.
 */

public class TaskAdapter extends ArrayAdapter<Task>{

    Context myContext;
    int myLayoutResourceId;
    ArrayList<Task> myTasks;

    ArrayList<Boolean> statesOfCheckBoxes;

    public TaskAdapter (Context context, int resource, ArrayList<Task> tasks){
        super(context,resource,tasks);
        this.myContext=context;
        this.myLayoutResourceId=resource;
        this.myTasks=tasks;
}
    @Override
    public View getView(int position,  View convertView, ViewGroup parent) {
        final ViewHolder holder;
        View taskView;
        // get a task view
        if(convertView!=null){
          holder = (ViewHolder) convertView.getTag();
          taskView = convertView;
        }

        LayoutInflater inflater = LayoutInflater.from(myContext);
        taskView = inflater.inflate(myLayoutResourceId,parent,false);

        TextView taskTitle = (TextView) taskView.findViewById(R.id.text_view);
        ImageButton deleteButton = (ImageButton) taskView.findViewById(R.id.delete_button);
        CheckBox checkBox = (CheckBox) taskView.findViewById(R.id.check_box);

        Task task = myTasks.get(position);
        taskTitle.setText(task.getTitle());

        deleteButton.setTag(position);
        deleteButton.setFocusable(false);
        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(getContext(),"Delete Button",Toast.LENGTH_SHORT).show();
                int whichTask = (Integer)v.getTag();
                Task taskToDelete = myTasks.get(whichTask);
                ParameterType param =  new ParameterType(2,taskToDelete,null);
                new ServerConnection().execute(param);

                Information info = new Information("","","","","","","",UserHomePage.taskListID,-1);
                param = new ParameterType(0,null,info); // httpRequestMode = 0 for refresh the list
                new ServerConnection().execute(param);

            }
        });

        checkBox.setTag(position);
        checkBox.setFocusable(false);
        checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int whichTask = (Integer)v.getTag();
                //Log.d("Debug",whichTask +"th item's checkbox");
                Task taskToChange = myTasks.get(whichTask);
                taskToChange.setChecked(!taskToChange.isChecked());
                ParameterType param =  new ParameterType(3,taskToChange,null);
                new ServerConnection().execute(param);
            }
        });
        checkBox.setChecked(task.isChecked());
        return taskView;
    }
}
