package com.bil495.todolist;

/**
 * Created by Ertuğrul Güngör on 02.08.2017.
 */

public class Information {
    private String username;            // used in registration and login
    private String password;            // used in registration and login
    private String passwordAgain;       // used in registration only
    private String name;                // used in registration only
    private String surname;             // used in registration only
    private String email;               // used in registration only
    private String taskListName;        // only used in adding list
    private int taskListID;             // used in fetching, adding, deleting, checking tasks
    private int userID;                 // used in fetching and adding list

    public Information(String username, String password, String passwordAgain, String name,
                       String surname, String email, String taskListName, int taskListID, int userID) {
        this.username = username;
        this.password = password;
        this.passwordAgain = passwordAgain;
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.taskListName = taskListName;
        this.taskListID = taskListID;
        this.userID = userID;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getPasswordAgain() {
        return passwordAgain;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getEmail() {
        return email;
    }

    public String getTaskListName() {
        return taskListName;
    }

    public int getTaskListID() {
        return taskListID;
    }

    public int getUserID() {
        return userID;
    }
}
