package com.bil495.todolist;
import android.os.AsyncTask;
import android.renderscript.ScriptGroup;
import android.speech.tts.Voice;
import android.support.v4.os.AsyncTaskCompat;
import android.util.Log;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Array;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by Ertuğrul Güngör on 14.07.2017.
 */

public class ServerConnection extends AsyncTask <ParameterType,Void,Integer> {
    int mode;
    int userID;
    /*
    private HttpURLConnection getConnection(int httpRequestMode,int taskID){
        HttpURLConnection conn = null;
        URL url ;
        try {
            if (httpRequestMode == 0) {
                url = new URL("https://still-lake-89097.herokuapp.com/tasks/fetchTasks/task_list_id=1");
                conn = (HttpURLConnection) url.openConnection();
                conn.setDoInput(true);
                conn.setRequestMethod("GET");
            }
        }
        catch(Exception e){
            Log.e("HTTP Handler", "Exception: " + e.getMessage());
        }

        return conn;
    }
    */
    private String convertStreamToString(InputStream inputStream) {

        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder sb = new StringBuilder();

        String readLine;
        try {
            while ((readLine = reader.readLine()) != null) {
                sb.append(readLine).append('\n');
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected Integer doInBackground(ParameterType... parameters) {
        String responseString="";
            if (parameters[0].httpRequestMode==0){     // connect server to fetch data
                try {
                    //HttpURLConnection myConnection = getConnection(parameters[0].httpRequestMode,-1);
                    HttpURLConnection myConnection;

                    URL url = new URL("http://still-lake-89097.herokuapp.com/tasks/fetchTasks/" +
                            "task_list_id="+parameters[0].paramInfo.getTaskListID());

                    myConnection = (HttpURLConnection) url.openConnection();
                    //System.out.println("ID:"+parameters[0].paramInfo.getTaskListID());
                    myConnection.setDoInput(true);
                    myConnection.setRequestMethod("GET");
                    InputStream inputStream=null;
                    inputStream = new BufferedInputStream(myConnection.getInputStream());
                    //JSONObject jsonObject = new JSONObject(jsonString);
                    //JSONArray tasks = jsonObject.getJSONArray("");
                    String jsonString = convertStreamToString(inputStream);
                    //System.out.println(jsonString);

                    JSONArray tasks = new JSONArray(jsonString);
                    //ArrayList<Task> newTaskList = new ArrayList<Task>();
                    UserHomePage.myTaskNames.clear();

                    for (int i = 0; i < tasks.length(); i++) {
                        //System.out.println("bişeyler2");
                        JSONObject t = tasks.getJSONObject(i);
                        int id = t.getInt("id");
                        String title = t.getString("title");
                        String description = t.getString("description");
                        //String date = t.getString("date");
                        //String time = t.getString("time");
                        String due_date = t.getString("due_date");

                        String date,time;
                        Calendar cal = Calendar.getInstance();
                        if(due_date!="null"){
                            Log.d("If Debug",due_date);
                            date = due_date.substring(0,due_date.indexOf("T"));
                            time = due_date.substring(due_date.indexOf("T")+1,due_date.indexOf("T")+6);
                            String[] dateParts = date.split("-");
                            String[] timeParts = time.split(":");
                            cal.set(Integer.parseInt(dateParts[0]), Integer.parseInt(dateParts[1])-1, Integer.parseInt(dateParts[2]),
                                    Integer.parseInt(timeParts[0]), Integer.parseInt(timeParts[1]));
                        }
                        else{
                            cal.set(Calendar.YEAR, cal.getActualMinimum(Calendar.YEAR));
                            cal.set(Calendar.DAY_OF_MONTH, cal.getActualMinimum(Calendar.DAY_OF_MONTH));
                            cal.set(Calendar.MONTH, cal.getActualMinimum(Calendar.MONTH));
                            cal.set(Calendar.HOUR_OF_DAY, cal.getActualMinimum(Calendar.HOUR_OF_DAY));
                            cal.set(Calendar.MINUTE, cal.getActualMinimum(Calendar.MINUTE));

                        }
                        //System.out.println("bişeyler3");
                        boolean check = t.getBoolean("checked");
                        boolean notify = t.getBoolean("notify");
                        /*
                        Log.e("Debug", id+"");
                        Log.e("Debug", time);
                        Log.e("Debug", date);
                        Log.e("Debug", check+"");
                        */

                        Task task = new Task(id,title,description,cal,notify,check);
                        //System.out.println("flag*:"+task.getTitle());
                        UserHomePage.myTaskNames.add(task);
                    }

                } catch (final JSONException e) {
                    Log.e("Server Connection", "Json parsing error: " + e.getMessage());
                } catch (IOException e){
                    e.printStackTrace();
                }
                return 1;
            }
            else if (parameters[0].httpRequestMode==1){       // send a json with http POST to add task
                JSONObject jsonToSend = new JSONObject();
                Task newTask = parameters[0].paramTask;
                HttpClient client;HttpPost request;HttpResponse response=null;
                try {
                    Calendar c = newTask.getDueDate();
                    int year = c.get(Calendar.YEAR);
                    int month = c.get(Calendar.MONTH)/*+1*/;
                    int day = c.get(Calendar.DAY_OF_MONTH);
                    int hour = c.get(Calendar.HOUR_OF_DAY);
                    int minute = c.get(Calendar.MINUTE);
                    String date = year+"."+month+"."+day;
                    String time = hour+":"+minute;
                    jsonToSend.put("task_list", parameters[0].paramInfo.getTaskListID());
                    jsonToSend.put("task_title", newTask.getTitle());
                    jsonToSend.put("task_detail", newTask.getDescription());
                    //jsonToSend.put("date", date);
                    //jsonToSend.put("time", time);
                    jsonToSend.put("task_due_date", date+" / "+time);
                    jsonToSend.put("task_check", newTask.isChecked());
                    jsonToSend.put("task_notify", newTask.isNotify());

                    client = new DefaultHttpClient();
                    request = new HttpPost("http://still-lake-89097.herokuapp.com/tasks/addTask");

                    StringEntity se = new StringEntity( jsonToSend.toString());
                    se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                    request.setEntity(se);
                    response = client.execute(request);
                    HttpEntity entity = response.getEntity();
                    InputStream in = entity.getContent();
                    responseString = convertStreamToString(in);
                    JSONObject myObject = new JSONObject(responseString);
                    responseString = myObject.getString("status");
                    Log.d("** Debug Response ** ",responseString);
                   // Log.d("** Debug Response ** ",myObject.getString("msg"));
                }
                catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                }
                catch (IOException e) {
                        e.printStackTrace();
                }
                catch (JSONException e) {
                        e.printStackTrace();
                }
                if (responseString.equals("success"))
                    return 1;
                else
                    return -1;
            }
            else if (parameters[0].httpRequestMode==2) {    // send a http GET to delete specified task
                Log.e("Debug", parameters[0].paramTask.getTaskID() + "");
                //HttpURLConnection myConnection = getConnection(parameters[0].httpRequestMode,parameters[0].paramTask.getTaskID());
                URL url = null;
                try {
                    url = new URL("http://still-lake-89097.herokuapp.com/tasks/deleteTask/task_id=" + parameters[0].paramTask.getTaskID());
                    HttpResponse response = null;
                    HttpClient httpclient = new DefaultHttpClient();
                    HttpGet httpget = new HttpGet(url.toString());
                    response = httpclient.execute(httpget);
                    HttpEntity entity = response.getEntity();
                    InputStream in = entity.getContent();
                    responseString = convertStreamToString(in);
                    JSONObject myObject = new JSONObject(responseString);
                    responseString = myObject.getString("status");
                    Log.d("** Debug Response ** ",responseString);
                    //Log.d("** Debug Response ** ",myObject.getString("msg"));
                }
                catch (MalformedURLException e) {
                    e.printStackTrace();
                }
                catch (IOException e) {
                    e.printStackTrace();
                }
                catch (JSONException e) {
                    e.printStackTrace();
                }
                if (responseString.equals("success"))
                    return 1;
                else
                    return -1;
            }
            else if (parameters[0].httpRequestMode==3) {     // send a http GET to mark specified task as checked or unchecked
                URL url = null;
                try {
                    url = new URL("http://still-lake-89097.herokuapp.com/tasks/checkTask/task_id="
                            + parameters[0].paramTask.getTaskID() + "/checked=" + parameters[0].paramTask.isChecked());
                    HttpResponse response = null;
                    HttpClient httpclient = new DefaultHttpClient();
                    HttpGet httpget = new HttpGet(url.toString());
                    response = httpclient.execute(httpget);
                    HttpEntity entity = response.getEntity();
                    InputStream in = entity.getContent();
                    responseString = convertStreamToString(in);
                    JSONObject myObject = new JSONObject(responseString);
                    responseString = myObject.getString("status");
                    Log.d("** Debug Response ** ", responseString);
                    //Log.d("** Debug Response ** ",myObject.getString("msg"));
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (responseString.equals("success"))
                    return 1;
                else
                    return -1;
            }
            else if (parameters[0].httpRequestMode==4){
                // registration post
                mode=0;
                URL url = null;
                HttpResponse response=null;
                HttpClient httpclient = new DefaultHttpClient();
                HttpPost httppost=null;
                StringEntity se = null;
                JSONObject jsonToSend = new JSONObject();
                try {
                    url = new URL("http://still-lake-89097.herokuapp.com/users/register");

                    jsonToSend.put("username", parameters[0].paramInfo.getUsername());
                    jsonToSend.put("password",parameters[0].paramInfo.getPassword());
                    jsonToSend.put("againpassword",parameters[0].paramInfo.getPassword());
                    jsonToSend.put("name",parameters[0].paramInfo.getName());
                    jsonToSend.put("surname",parameters[0].paramInfo.getSurname());
                    jsonToSend.put("email",parameters[0].paramInfo.getEmail());
                    se = new StringEntity( jsonToSend.toString());
                    se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                    httppost = new HttpPost(url.toString());
                    httppost.setEntity(se);
                    response = httpclient.execute(httppost);

                    HttpEntity entity = response.getEntity();
                    InputStream in = entity.getContent();
                    responseString = convertStreamToString(in);
                    //System.out.println("Result:" + responseString);

                    JSONObject myObject = new JSONObject(responseString);
                    responseString = myObject.getString("status");
                    //userID = myObject.getInt("userid");
                    Log.d("** Debug Response ** ",responseString);
                    //Log.d("** Debug Response ** ",myObject.getString("msg"));
                }
                catch (MalformedURLException e) {
                    e.printStackTrace();
                }
                catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (responseString.equals("success")){
                    Register.getResponse(1);
                    return 1;
                }
                else
                    return -1;
            }
            else if (parameters[0].httpRequestMode==5){
                mode=1;
                // login get
                URL url = null;
                HttpResponse response=null;
                HttpClient httpclient = new DefaultHttpClient();
                HttpGet httpget=null;
                StringEntity se = null;
                JSONObject jsonToSend = new JSONObject();
                try {
                    url = new URL("http://still-lake-89097.herokuapp.com/login/username="
                            +parameters[0].paramInfo.getUsername()+ "/password="+parameters[0].paramInfo.getPassword());
                    /*
                    jsonToSend.put("username", parameters[0].paramInfo.getUsername());
                    jsonToSend.put("password",parameters[0].paramInfo.getPassword());
                    se = new StringEntity( jsonToSend.toString());
                    se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                    */
                    httpget = new HttpGet(url.toString());
                    //httppost.setEntity(se);
                    //response = httpclient.execute(httppost);
                    response = httpclient.execute(httpget);

                    HttpEntity entity = response.getEntity();
                    InputStream in = entity.getContent();
                    responseString = convertStreamToString(in);
                    //.out.println("Result:" + responseString);

                    JSONObject myObject = new JSONObject(responseString);
                    responseString = myObject.getString("status");
                    userID = myObject.getInt("user_id");
                    Log.d("** Debug Response ** ",responseString);
                    //Log.d("** Debug Response ** ",myObject.getString("msg"));
                }
                catch (MalformedURLException e) {
                    e.printStackTrace();
                }
                catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (responseString.equals("success")){
                    //System.out.println("succesfully logged in ");
                    MainActivity.getResponse(1,userID);
                    return 1;
                }
                else
                    return -1;
            }
            else if (parameters[0].httpRequestMode==6) {     // fetch task lists
                URL url = null;
                HttpResponse response=null;
                HttpClient httpclient = new DefaultHttpClient();
                HttpPost httppost=null;
                StringEntity se = null;
                JSONObject jsonToSend = new JSONObject();
                try {
                    url = new URL("http://still-lake-89097.herokuapp.com/task_lists/fetchLists");
                    jsonToSend.put("task_user_id", parameters[0].paramInfo.getUserID());
                    se = new StringEntity( jsonToSend.toString());
                    se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));

                    httppost = new HttpPost(url.toString());
                    httppost.setEntity(se);
                    response = httpclient.execute(httppost);

                    HttpEntity entity = response.getEntity();
                    InputStream in = entity.getContent();
                    responseString = convertStreamToString(in);
                    JSONObject myObject = new JSONObject(responseString);
                    responseString = myObject.getString("lists");
                    Log.d("**Response:TaskLists**",responseString);
                    JSONArray taskLists = new JSONArray(responseString);
                    //userID = myObject.getInt("user_id");
                    if(myObject.getString("status").equals("success")){
                        UserHomePage.myTaskLists.clear();
                        for (int i = 0; i < taskLists.length(); i++) {
                            JSONObject t = taskLists.getJSONObject(i);
                            String title = t.getString("title");
                            //System.out.println("**title:** " + title);
                            int id = t.getInt("id");
                            TaskList list = new TaskList(id,title);
                            UserHomePage.myTaskLists.add(list);
                        }
                        //System.out.println("Flag:"+myObject.getInt("lastusedList"));
                        UserHomePage.getResponse(myObject.getInt("lastusedList"));
                    }
                    else
                        return -1;
                }
                catch (MalformedURLException e) {
                    e.printStackTrace();
                }
                catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return 1;
            }

            else if (parameters[0].httpRequestMode==7){      // to add new task list
                URL url = null;
                HttpResponse response=null;
                HttpClient httpclient = new DefaultHttpClient();
                HttpPost httppost=null;
                StringEntity se = null;
                JSONObject jsonToSend = new JSONObject();
                try {
                    url = new URL("http://still-lake-89097.herokuapp.com/task_lists/addList");
                    jsonToSend.put("list_name", parameters[0].paramInfo.getTaskListName());
                    jsonToSend.put("task_user_id", parameters[0].paramInfo.getUserID());

                    se = new StringEntity( jsonToSend.toString());
                    se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                    httppost = new HttpPost(url.toString());
                    httppost.setEntity(se);
                    response = httpclient.execute(httppost);

                    HttpEntity entity = response.getEntity();
                    InputStream in = entity.getContent();
                    responseString = convertStreamToString(in);
                    JSONObject myObject = new JSONObject(responseString);
                    responseString = myObject.getString("status");
                    //Log.d("** Debug Response ** ",responseString);
                    //Log.d("** Debug Response ** ",myObject.getString("msg"));
                }
                catch (MalformedURLException e) {
                    e.printStackTrace();
                }
                catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (responseString.equals("success")){
                    Register.getResponse(1);
                    return 1;
                }
                else
                    return -1;
            }
            else if (parameters[0].httpRequestMode==8){
                URL url = null;
                HttpResponse response=null;
                HttpClient httpclient = new DefaultHttpClient();
                HttpPost httppost=null;
                StringEntity se = null;
                JSONObject jsonToSend = new JSONObject();
                try {
                    url = new URL("http://still-lake-89097.herokuapp.com/tasks/editTask");

                    jsonToSend.put("task_id",parameters[0].paramTask.getTaskID());

                    Calendar c = parameters[0].paramTask.getDueDate();
                    int year = c.get(Calendar.YEAR);
                    int month = c.get(Calendar.MONTH)+1;
                    int day = c.get(Calendar.DAY_OF_MONTH);
                    int hour = c.get(Calendar.HOUR_OF_DAY);
                    int minute = c.get(Calendar.MINUTE);
                    String date = year+"."+month+"."+day;
                    String time = hour+":"+minute;

                    jsonToSend.put("task_date", date+" / "+time);
                    jsonToSend.put("task_detail",parameters[0].paramTask.getDescription());
                    jsonToSend.put("task_title",parameters[0].paramTask.getTitle());
                    jsonToSend.put("task_notify", parameters[0].paramTask.isNotify());


                    se = new StringEntity( jsonToSend.toString());
                    se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                    httppost = new HttpPost(url.toString());
                    httppost.setEntity(se);
                    response = httpclient.execute(httppost);

                    /*
                    HttpEntity entity = response.getEntity();
                    InputStream in = entity.getContent();
                    responseString = convertStreamToString(in);
                    JSONObject myObject = new JSONObject(responseString);
                    responseString = myObject.getString("status");
                    */
                }
                catch (MalformedURLException e) {
                    e.printStackTrace();
                }
                catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                /*
                if (responseString.equals("success")){
                    Register.getResponse(1);
                    return 1;
                }

                else
                    return -1;
                */
                return 1;
            }
        return -1;
    }

    // change it
    protected void onPostExecute(Integer serverStatus) {
        /*
        super.onPostExecute(aVoid);
        System.out.println("onPostExecute");
        System.out.println("status:" +serverStatus) ;
        int status = serverStatus;
        if(mode==0){
            MainActivity.getResponse(status,userID);
        }
        else if (mode==1){
            Register.getResponse(status);
        }
        */

        /* after fetching lists or tasks, update views */
        if (UserHomePage.navigationBarAdapter!=null ){
            UserHomePage.taskListNames.clear();
            for(int i=0;i<UserHomePage.myTaskLists.size();i++){
                UserHomePage.taskListNames.add(UserHomePage.myTaskLists.get(i).getListTitle());
            }
            UserHomePage.navigationBarAdapter.notifyDataSetChanged();

            //System.out.println("?? Navigation bar notified ??");
        }
        if (UserHomePage.mainPageAdapter!=null ){
            //System.out.println("?? main panel notified ??");
            UserHomePage.mainPageAdapter.notifyDataSetChanged();
        }

    }
}
