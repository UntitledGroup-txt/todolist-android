package com.bil495.todolist;

import java.text.DateFormat;
import java.util.Date;
import java.util.Calendar;
/**
 * Created by Ertuğrul Güngör on 11.07.2017.
 */

public class Task {
    private int taskID;
    private String title;
    private String description;
    private Calendar dueDate;
    private boolean notify;
    private boolean checked;

    public int getTaskID() { return taskID; }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public Calendar getDueDate() {
        return dueDate;
    }

    public boolean isNotify() {
        return notify;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setDueDate(Calendar dueDate) {
        this.dueDate = dueDate;
    }

    public void setNotify(boolean notify) {
        this.notify = notify;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public void setTaskID(int taskID) { this.taskID = taskID; }


    public Task(int taskID,String title, String description, Calendar dueDate, boolean notify, boolean checked){
    this.checked=checked;
    this.notify=notify;
    this.dueDate=dueDate;
    this.title=title;
    this.description=description;
    this.taskID=taskID;

}

}
