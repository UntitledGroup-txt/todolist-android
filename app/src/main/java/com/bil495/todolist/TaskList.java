package com.bil495.todolist;

/**
 * Created by Ertuğrul Güngör on 02.08.2017.
 */

public class TaskList {
    private int listID;
    private String listTitle;

    public TaskList(int listID, String listTitle) {
        this.listID = listID;
        this.listTitle = listTitle;
    }

    public int getListID() {
        return listID;
    }

    public String getListTitle() {
        return listTitle;
    }
}

