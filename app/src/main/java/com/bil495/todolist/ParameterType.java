package com.bil495.todolist;

/**
 * Created by Ertuğrul Güngör on 16.07.2017.
 */
public class ParameterType{
    int httpRequestMode;
    Task paramTask;
    Information paramInfo;

    public ParameterType(int httpRequestMode,Task paramTask,Information paramInfo){
        this.httpRequestMode=httpRequestMode;
        this.paramTask=paramTask;
        this.paramInfo=paramInfo;
    }
}
