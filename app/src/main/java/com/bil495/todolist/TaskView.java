package com.bil495.todolist;

import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.concurrent.ExecutionException;

import static android.view.Gravity.CENTER;


/**
 * Created by Ertuğrul Güngör on 12.07.2017.
 */

public class TaskView extends AppCompatActivity{
    Task shownTask;
    PopupWindow editTaskPopup;
    EditText taskTitleEdit;
    EditText taskDescriptionEdit ;
    EditText taskDate;
    EditText taskTime;
    CheckBox notify;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.task_view);
        final int whichTask = getIntent().getIntExtra(UserHomePage.EXTRA_MESSAGE,-1);
        //shownTask = UserHomePage.myTaskNames[whichTask];
        shownTask = UserHomePage.myTaskNames.get(whichTask);

        TextView title = (TextView) findViewById(R.id.title);
        TextView description = (TextView) findViewById(R.id.description);
        TextView due_date = (TextView) findViewById(R.id.due_date);

        title.setText(shownTask.getTitle());
        description.setText(shownTask.getDescription());

        ToggleButton toggleButton = (ToggleButton) findViewById(R.id.toggle_button);
        toggleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shownTask.setNotify(!shownTask.isNotify());
                Task newTask = new Task(shownTask.getTaskID(),shownTask.getTitle(),shownTask.getDescription()
                        ,shownTask.getDueDate(),shownTask.isNotify(),shownTask.isChecked());
                ParameterType param =  new ParameterType(8,newTask,null);
                new ServerConnection().execute(param);
            }
        });
        toggleButton.setChecked(shownTask.isNotify());


        ImageButton editButton = (ImageButton) findViewById(R.id.edit_button);

        // replace
        editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LayoutInflater layoutInflater = (LayoutInflater) getBaseContext().getSystemService(LAYOUT_INFLATER_SERVICE);
                final View editPopupView = layoutInflater.inflate(R.layout.add_task_popup,null);
                DisplayMetrics displayMetrics = new DisplayMetrics();
                getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                int height = (int)(displayMetrics.heightPixels*0.75);
                int width = (int)(displayMetrics.widthPixels*0.9);

                editTaskPopup = new PopupWindow(editPopupView, width,height);
                RelativeLayout popupPosition;
                popupPosition = (RelativeLayout) findViewById(R.id.task_view_layout);
                editTaskPopup.setFocusable(true);
                editTaskPopup.update();
                editTaskPopup.showAtLocation(popupPosition, CENTER,0,0);

                taskTitleEdit = (EditText) editPopupView.findViewById(R.id.task_title_edit);
                taskDescriptionEdit = (EditText) editPopupView.findViewById(R.id.task_description_edit);
                taskDate = (EditText) editPopupView.findViewById(R.id.task_date_edit);
                taskTime = (EditText) editPopupView.findViewById(R.id.task_time_edit);
                notify = (CheckBox) editPopupView.findViewById(R.id.add_notify);

                /* show user to current informations of task */
                taskTitleEdit.setText(shownTask.getTitle());
                taskDescriptionEdit.setText(shownTask.getDescription());
                int year = shownTask.getDueDate().get(Calendar.YEAR);
                int month = shownTask.getDueDate().get(Calendar.MONTH)/*+1*/;
                int day = shownTask.getDueDate().get(Calendar.DAY_OF_MONTH);
                int hour = shownTask.getDueDate().get(Calendar.HOUR_OF_DAY);
                int minute = shownTask.getDueDate().get(Calendar.MINUTE);
                if(year==shownTask.getDueDate().getMinimum(Calendar.YEAR)
                        && month-1==shownTask.getDueDate().getMinimum(Calendar.MONTH)
                        && day==shownTask.getDueDate().getMinimum(Calendar.DAY_OF_MONTH)
                        && hour==shownTask.getDueDate().getMinimum(Calendar.HOUR_OF_DAY)
                        && minute==shownTask.getDueDate().getMinimum(Calendar.MINUTE))
                {
                    taskDate.setText("");
                    taskTime.setText("");

                }
                else{
                    taskDate.setText(year+"."+month+"."+day);
                    taskTime.setText(hour+":"+minute);
                }


                notify.setChecked(shownTask.isNotify());

                notify.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                    }
                });


                ImageButton cancelButton = (ImageButton) editPopupView.findViewById(R.id.cancel_button);
                cancelButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        editTaskPopup.dismiss();
                    }
                });

                ImageButton saveButton = (ImageButton) editPopupView.findViewById(R.id.save_button);
                saveButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        String title = taskTitleEdit.getText().toString();
                        String description = taskDescriptionEdit.getText().toString();
                        String date = taskDate.getText().toString();
                        System.out.println("Date Edit:"+date);
                        String time = taskTime.getText().toString();
                        Calendar cal = Calendar.getInstance();
                        boolean willBeNotified = notify.isChecked();

                        if (date.length()==0 && time.length()==0) {
                            cal.set(Calendar.YEAR, cal.getActualMinimum(Calendar.YEAR));
                            cal.set(Calendar.DAY_OF_MONTH, cal.getActualMinimum(Calendar.DAY_OF_MONTH));
                            cal.set(Calendar.MONTH, cal.getActualMinimum(Calendar.MONTH));
                            cal.set(Calendar.HOUR_OF_DAY, cal.getActualMinimum(Calendar.HOUR_OF_DAY));
                            cal.set(Calendar.MINUTE, cal.getActualMinimum(Calendar.MINUTE));

                            Task newTask = new Task(shownTask.getTaskID(),title,description,cal,willBeNotified,shownTask.isChecked());
                            Information info = new Information("","","","","","","",UserHomePage.taskListID,-1);
                            ParameterType param = new ParameterType(8,newTask,info); // httpRequestMode = 8 for editing task
                            new ServerConnection().execute(param);

                            param = new ParameterType(0,null,info); // httpRequestMode = 0 for refresh the list
                            new ServerConnection().execute(param);
                            //mainPageAdapter.notifyDataSetChanged();
                            editTaskPopup.dismiss();

                            Intent intent = new Intent(getBaseContext(),UserHomePage.class);
                            intent.putExtra("userID",getIntent().getIntExtra("userID",-1));
                            intent.putExtra("username",getIntent().getStringExtra("username"));
                            startActivity(intent);
                        }
                        else if(date.length()<=10 && time.length()<=5) {
                            String[] dateParts = date.split("\\.");
                            String[] timeParts = time.split(":");
                            if (dateParts.length == 3 && timeParts.length == 2) {
                                if(Integer.parseInt(dateParts[1])>=1 && Integer.parseInt(dateParts[1])<=12 ){
                                    if(Integer.parseInt(dateParts[2])>=1 && Integer.parseInt(dateParts[2])<=31){
                                        cal.set(Integer.parseInt(dateParts[0]), Integer.parseInt(dateParts[1]), Integer.parseInt(dateParts[2]),
                                                Integer.parseInt(timeParts[0]), Integer.parseInt(timeParts[1]));

                                        Task newTask = new Task(shownTask.getTaskID(),title,description,cal,willBeNotified,false);
                                        Information info = new Information("","","","","","","",UserHomePage.taskListID,-1);
                                        ParameterType param = new ParameterType(8,newTask,info); // httpRequestMode = 8 for editing task
                                        new ServerConnection().execute(param);

                                        param = new ParameterType(0,null,info); // httpRequestMode = 0 for refresh the list
                                        new ServerConnection().execute(param);
                                        //mainPageAdapter.notifyDataSetChanged();
                                        editTaskPopup.dismiss();

                                        Intent intent = new Intent(getBaseContext(),UserHomePage.class);
                                        intent.putExtra("userID",getIntent().getIntExtra("userID",-1));
                                        intent.putExtra("username",getIntent().getStringExtra("username"));
                                        startActivity(intent);
                                    }
                                    else{
                                        Toast.makeText(TaskView.this, "Please be careful about the format of date and time", Toast.LENGTH_SHORT).show();
                                    }
                                }
                                else{
                                    Toast.makeText(TaskView.this, "Please be careful about the format of date and time", Toast.LENGTH_SHORT).show();
                                }

                            }
                            else {
                                Toast.makeText(TaskView.this, "Please be careful about the format of date and time", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                });
            }
        });
        // replace

        int year = shownTask.getDueDate().get(Calendar.YEAR);
        int month = shownTask.getDueDate().get(Calendar.MONTH)/*+1*/;     /* +1 need problem. */
        int day = shownTask.getDueDate().get(Calendar.DAY_OF_MONTH);
        int hour = shownTask.getDueDate().get(Calendar.HOUR_OF_DAY);
        int minute = shownTask.getDueDate().get(Calendar.MINUTE);
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR,year );
        cal.set(Calendar.DAY_OF_MONTH,day);
        cal.set(Calendar.MONTH,month-1 );
        cal.set(Calendar.HOUR_OF_DAY,hour);
        cal.set(Calendar.MINUTE, minute);

        System.out.println("TaskView Debug"+year+"  "+month+"  "+day+" - "+hour+"  "+minute);
        if(year==shownTask.getDueDate().getMinimum(Calendar.YEAR)
                && month-1==shownTask.getDueDate().getMinimum(Calendar.MONTH)
                && day==shownTask.getDueDate().getMinimum(Calendar.DAY_OF_MONTH)
                && hour==shownTask.getDueDate().getMinimum(Calendar.HOUR_OF_DAY)
                && minute==shownTask.getDueDate().getMinimum(Calendar.MINUTE))
        {
            due_date.setText("∞");
        }
        else{
            SimpleDateFormat format = new SimpleDateFormat("yyyy.MM.dd / hh:mm");
            String formatted_due_date = format.format(cal.getTime());

            //due_date.setText(year+"."+month+"."+day+" - "+hour+":"+minute);
            due_date.setText(formatted_due_date);
        }


    }

}
