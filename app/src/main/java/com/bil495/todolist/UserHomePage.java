/**
 * Created by Ertuğrul Güngör on 01.08.2017.
 */
package com.bil495.todolist;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.firebase.messaging.FirebaseMessaging;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.concurrent.ExecutionException;

import static android.view.Gravity.CENTER;

public class UserHomePage extends AppCompatActivity {

    public static final String EXTRA_MESSAGE = "com.bil495.todolist.Home";
    private DrawerLayout myDrawerLayout;
    private ListView sideMenuListView;
    private ActionBarDrawerToggle myDrawerToggle;
    private ListView taskListView;
    private ImageButton addButton;
    private PopupWindow addTaskPopup;
    private PopupWindow addTaskListPopup;
    private DrawerLayout popupPosition;
    static ArrayAdapter<String> navigationBarAdapter;
    static TaskAdapter mainPageAdapter;
    static ArrayList<Task> myTaskNames;
    static ArrayList<String> taskListNames;
    static ArrayList<TaskList> myTaskLists;
    private String username_logged_in ;
    private int userID;
    static int taskListID;      // to be reached from other classes when refreshing tasks needed.(e.g after deleting)
    static int lastUsedListID;  // to get last used task list initially
    private ImageButton addListButton;
    CheckBox notify;
    boolean willBeNotified;
    @Override
        protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_home_page);

        // Required for push notifications
        username_logged_in = getIntent().getStringExtra("username");
        System.out.println("username which has just logged in : "+username_logged_in);
        FirebaseMessaging.getInstance().subscribeToTopic(username_logged_in);

        // arraylist for tasks selected from any list (last used one for initial)
        myTaskNames = new ArrayList<Task>();
        // arraylist for all task lists
        myTaskLists = new ArrayList<TaskList>();
        // reach UI elements
        taskListView = (ListView) findViewById(R.id.task_list);
        myDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        sideMenuListView = (ListView) findViewById(R.id.drawer_list);

        // create navigation drawer
        myDrawerToggle = new ActionBarDrawerToggle(this, myDrawerLayout, R.string.open, R.string.close);
        myDrawerToggle.syncState();
        myDrawerLayout.addDrawerListener(myDrawerToggle);

        /*
        final String dummyTaskListNames[] = {
                "Task List Name 1",
                "Task List Name 2",
                "Task List Name 3"
        };
        */

        //ArrayAdapter<String> sideMenuAdapter = new ArrayAdapter<String>(getBaseContext(),android.R.layout.,dummyTaskListNames);
        //sideMenuListView.setAdapter(sideMenuAdapter);


        /*
        sideMenuListView.setOnScrollListener(new AbsListView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                if (scrollState == SCROLL_STATE_IDLE) {
                    sideMenuListView.bringToFront();
                    myDrawerLayout.requestLayout();
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            }
        });
        */


        /* fetch all task lists for user who is logged in*/
        userID = getIntent().getIntExtra("userID",-1);
        Information info = new Information("","","","","","","",-1,userID);
        ParameterType param = new ParameterType(6,null,info);
        try {
            new ServerConnection().execute(param).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        taskListID = lastUsedListID; // current list is the last used one at first
        /* after fetching names of task lists, put them to navigation drawer */
        //sideMenuListView.setAdapter(new ArrayAdapter<TaskList>(this,R.layout.drawer_list_item, myTaskListsNames));

        // TODO: do it whenever a list is added (by notifying after taskListNames arraylist is changed)
        taskListNames = new ArrayList<String>();
        for(int i=0;i<myTaskLists.size();i++){
            taskListNames.add(myTaskLists.get(i).getListTitle());
        }
        navigationBarAdapter = new ArrayAdapter<String>(this,R.layout.drawer_list_item,taskListNames);
        sideMenuListView.setAdapter(navigationBarAdapter);
        // set click listener for navigation drawer (what to do once a task list is selected)
        sideMenuListView.setOnItemClickListener(new DrawerItemClickListener());

        /* after fetching names of tasks, put them to screen (main panel) */
        mainPageAdapter = new TaskAdapter(getApplicationContext(),R.layout.task,myTaskNames);
        taskListView.setAdapter(mainPageAdapter);

        /* fetch all tasks from list that is shown last*/
        info = new Information("","","","","","","",taskListID,-1);
        param = new ParameterType(0,null,info); // httpRequestMode = 0 for fetching tasks
        new ServerConnection().execute(param);



        /* make upper left button open/close navigation drawer ??*/
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        taskListView.setTextFilterEnabled(true);

        /* open selected task's detailed page*/
        taskListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(view.getContext(),TaskView.class);
                intent.putExtra(EXTRA_MESSAGE,position);
                intent.putExtra("userID",userID);
                intent.putExtra("username",username_logged_in);
                startActivity(intent);
            }
        });


        ImageButton logoutButton = (ImageButton) findViewById(R.id.logout);
        logoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(UserHomePage.this, "Logout", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(getBaseContext(),MainActivity.class);
                startActivity(intent);
            }
        });
        TextView username = (TextView) findViewById(R.id.username_text);
        username.setText(username_logged_in);


        /* reach "add task" button and give it's functionality (i.e. eventListener) */
        addButton = (ImageButton) findViewById(R.id.add_button);
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LayoutInflater layoutInflater = (LayoutInflater) getBaseContext().getSystemService(LAYOUT_INFLATER_SERVICE);
                final View addPopupView = layoutInflater.inflate(R.layout.add_task_popup,null);

                DisplayMetrics displayMetrics = new DisplayMetrics();
                getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                int height = (int)(displayMetrics.heightPixels*0.75);
                int width = (int)(displayMetrics.widthPixels*0.9);

                //addTaskPopup = new PopupWindow(addPopupView, DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);
                addTaskPopup = new PopupWindow(addPopupView, width,height);

                popupPosition = (DrawerLayout) findViewById(R.id.drawer_layout);
                addTaskPopup.setFocusable(true);
                addTaskPopup.update();
                addTaskPopup.showAtLocation(popupPosition, CENTER,0,0);

                notify = (CheckBox) addPopupView.findViewById(R.id.add_notify);
                notify.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        willBeNotified=(!willBeNotified);
                    }
                });
                notify.setChecked(willBeNotified);

                ImageButton cancelButton = (ImageButton) addPopupView.findViewById(R.id.cancel_button);
                cancelButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        addTaskPopup.dismiss();
                    }
                });

                ImageButton saveButton = (ImageButton) addPopupView.findViewById(R.id.save_button);
                saveButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        EditText taskTitleEdit = (EditText) addPopupView.findViewById(R.id.task_title_edit);
                        EditText taskDescriptionEdit = (EditText) addPopupView.findViewById(R.id.task_description_edit);
                        EditText taskDate = (EditText) addPopupView.findViewById(R.id.task_date_edit);
                        EditText taskTime = (EditText) addPopupView.findViewById(R.id.task_time_edit);
                        willBeNotified = notify.isChecked();


                        String title = taskTitleEdit.getText().toString();
                        String description = taskDescriptionEdit.getText().toString();
                        String date = taskDate.getText().toString();
                        String time = taskTime.getText().toString();
                        Calendar cal = Calendar.getInstance();

                        if (date.length()==0 && time.length()==0) {
                            cal.set(Calendar.YEAR, cal.getActualMinimum(Calendar.YEAR));
                            cal.set(Calendar.DAY_OF_MONTH, cal.getActualMinimum(Calendar.DAY_OF_MONTH));
                            cal.set(Calendar.MONTH, cal.getActualMinimum(Calendar.MONTH));
                            cal.set(Calendar.HOUR_OF_DAY, cal.getActualMinimum(Calendar.HOUR_OF_DAY));
                            cal.set(Calendar.MINUTE, cal.getActualMinimum(Calendar.MINUTE));

                            Task newTask = new Task(-1,title,description,cal,willBeNotified,false);
                            Information info = new Information("","","","","","","",UserHomePage.taskListID,-1);
                            ParameterType param = new ParameterType(1,newTask,info); // httpRequestMode = 1 for adding task

                            new ServerConnection().execute(param);
                            param = new ParameterType(0,null,info); // httpRequestMode = 0 for refresh the list
                            new ServerConnection().execute(param);
                            //mainPageAdapter.notifyDataSetChanged();
                            addTaskPopup.dismiss();
                        }
                        else if(date.length()<=10 && time.length()<=5) {
                            String[] dateParts = date.split("\\.");
                            String[] timeParts = time.split(":");
                            if (dateParts.length == 3 && timeParts.length == 2) {
                                if(Integer.parseInt(dateParts[1])>=1 && Integer.parseInt(dateParts[1])<=12 ){
                                    if(Integer.parseInt(dateParts[2])>=1 && Integer.parseInt(dateParts[2])<=31){
                                        if(Integer.parseInt(timeParts[0])>=0 && Integer.parseInt(timeParts[0])<=23){
                                            if(Integer.parseInt(timeParts[1])>=0 && Integer.parseInt(timeParts[1])<=59){
                                                cal.set(Integer.parseInt(dateParts[0]), Integer.parseInt(dateParts[1])+1/**/, Integer.parseInt(dateParts[2]),
                                                        Integer.parseInt(timeParts[0]), Integer.parseInt(timeParts[1]));
                                                /* when user select a list from navigation drawer, taskListID is updated.
                                                * Therefore this task will be added to selected list as it is supposed to*/
                                                Task newTask = new Task(-1,title,description,cal,willBeNotified,false);
                                                Information info = new Information("","","","","","","",UserHomePage.taskListID,-1);
                                                ParameterType param = new ParameterType(1,newTask,info); // httpRequestMode = 1 for adding task

                                                new ServerConnection().execute(param);
                                                param = new ParameterType(0,null,info); // httpRequestMode = 0 for refresh the list
                                                new ServerConnection().execute(param);
                                                //mainPageAdapter.notifyDataSetChanged();
                                                addTaskPopup.dismiss();
                                            }
                                            else{
                                                Toast.makeText(UserHomePage.this, "Please be careful about minute of the given time", Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                        else{
                                            Toast.makeText(UserHomePage.this, "Please be careful about hour of the given time", Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                    else{
                                        Toast.makeText(UserHomePage.this, "Please be careful about day of the given date", Toast.LENGTH_SHORT).show();
                                    }
                                }
                                else{
                                    Toast.makeText(UserHomePage.this, "Please be careful about month of the date ", Toast.LENGTH_SHORT).show();
                                }
                            }
                            else {
                                Toast.makeText(UserHomePage.this, "Please be careful about the length of date and time", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                });
            }
        });


        addListButton = (ImageButton) findViewById(R.id.add_list_button);
        addListButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LayoutInflater layoutInflater = (LayoutInflater) getBaseContext().getSystemService(LAYOUT_INFLATER_SERVICE);
                final View addPopupView = layoutInflater.inflate(R.layout.add_list_popup,null);

                DisplayMetrics displayMetrics = new DisplayMetrics();
                getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                int height = (int)(displayMetrics.heightPixels*0.45);
                int width = (int)(displayMetrics.widthPixels*0.9);

                addTaskListPopup = new PopupWindow(addPopupView, width,height);

                popupPosition = (DrawerLayout) findViewById(R.id.drawer_layout);
                addTaskListPopup.setFocusable(true);
                addTaskListPopup.update();
                addTaskListPopup.showAtLocation(popupPosition, CENTER,0,0);

                ImageButton cancelButton = (ImageButton) addPopupView.findViewById(R.id.cancel_button_2);
                cancelButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        addTaskListPopup.dismiss();
                    }
                });

                ImageButton saveButton = (ImageButton) addPopupView.findViewById(R.id.save_button_2);
                saveButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        EditText listTitleEdit = (EditText) addPopupView.findViewById(R.id.list_title_edit);
                        String title = listTitleEdit.getText().toString();
                        if(title.equals("")){
                            //Toast.makeText(UserHomePage.this, "List title cannot be empty", Toast.LENGTH_SHORT).show();
                        }
                        else{
                            Toast.makeText(UserHomePage.this, "List title is valid", Toast.LENGTH_SHORT).show();
                            Information info = new Information("","","","","","",title,-1,userID);
                            ParameterType param = new ParameterType(7,null,info); // httpRequestMode = 7 for adding list
                            try {
                                new ServerConnection().execute(param).get();
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            } catch (ExecutionException e) {
                                e.printStackTrace();
                            }
                            /* fetch all task lists again for refreshing*/
                            info = new Information("","","","","","","",-1,userID);
                            param = new ParameterType(6,null,info);
                            try {
                                new ServerConnection().execute(param).get();
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            } catch (ExecutionException e) {
                                e.printStackTrace();
                            }
                            addTaskListPopup.dismiss();
                        }
                    }
                });

            }
        });
    }
    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            selectTaskList(position);
        }
    }

    void selectTaskList(int position){
        sideMenuListView.setItemChecked(position, true);
        myDrawerLayout.closeDrawers();
        taskListID = myTaskLists.get(position).getListID();
        //Toast.makeText(UserHomePage.this, "current list id is: "+taskListID, Toast.LENGTH_SHORT).show();
        Information info = new Information("","","","","","","",taskListID,-1);
        ParameterType param = new ParameterType(0,null,info); // httpRequestMode = 0 for fetching tasks
        new ServerConnection().execute(param);
        //Toast.makeText(UserHomePage.this, position+"", Toast.LENGTH_SHORT).show();
    }


    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        myDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggls
        myDrawerToggle.onConfigurationChanged(newConfig);
    }



    // tapping  options button at upper left shows the side menu
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (myDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public static void getResponse(int lastUsedListID){
        UserHomePage.lastUsedListID=lastUsedListID;
    }

}   //class


